package com.company;

import javax.swing.*;
import java.io.*;


public class Main {

    public static void main(String[] args) throws IOException {
        JFileChooser fileopen = new JFileChooser();
        int ret = fileopen.showDialog(null, "Открыть файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            String filePath = fileopen.getSelectedFile().getPath();
            String content = readFileAsString(filePath);
            //System.out.println(content);
            String encrypted = MCrypt.encrypt(content);
            writeInFile(encrypted, "C:/Users/Master/Desktop/text_enc.txt");
            //System.out.println(encrypted);
            String decrypted = MCrypt.decrypt(encrypted);
            writeInFile(decrypted, "C:/Users/Master/Desktop/text_dec.txt");
        }
    }

    private static String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

    private static void writeInFile(String text, String path) throws IOException {
        /*FileWriter writer = null;
        try{
            writer = new FileWriter(path, false);
            writer.write(text);
        }catch(FileNotFoundException e){
            System.out.println("Файл не найден");
        }*/

        Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(path), "UTF-8"));
        try {
            out.write(text);
        } finally {
            out.close();
        }
    }
}
