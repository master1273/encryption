package com.company;



/**
 * Created by Pavel on 31.07.2015.
 */
public class MCrypt {

    public static String encrypt(String string){

        char[] array = string.toCharArray();
        String newString = "";
        for (int i = 0; i < array.length; i++){
            newString += Character.toString((char) grayEncode((int)array[i]));
        }
        return newString;
    }

    public static String decrypt(String string){
        char[] array = string.toCharArray();
        String newString = "";
        for (int i = 0; i < array.length; i++){
            newString += Character.toString((char) grayDecode((int) array[i]));
        }
        return newString;
    }

    private String toBinary(int asciiCode) {
        return Integer.toBinaryString(asciiCode);
    }

    private int toDecimal(String binaryString) {
        return Integer.parseInt(binaryString, 2);
    }

    public static long grayEncode(long n){
        return n ^ (n >>> 1);
    }

    public static long grayDecode(long n) {
        long p = n;
        while ((n >>>= 1) != 0)
            p ^= n;
        return p;
    }
}
